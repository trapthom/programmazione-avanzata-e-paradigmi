package pap.exam;

import org.junit.Assert;
import org.junit.Test;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Optional;

/**
 * Created by thomas on 24/04/17.
 */
public class CounterStreamsTest {

    @Test
    public void inc() throws Exception {
        List<Counter> counterList = Arrays.asList(
                new Counter(1),
                new Counter(0),
                new Counter(2),
                new Counter(-1),
                new Counter(0)
        );

        CounterStreams.inc(counterList);

        Assert.assertEquals(1, counterList.get(0).getValue());
        Assert.assertEquals(1, counterList.get(1).getValue());
        Assert.assertEquals(2, counterList.get(2).getValue());
        Assert.assertEquals(-1, counterList.get(3).getValue());
        Assert.assertEquals(1, counterList.get(4).getValue());
    }

    @Test
    public void findMax() throws Exception {
        List<Counter> counterList = Arrays.asList(
                new Counter(10),
                new Counter(15),
                new Counter(20),
                new Counter(25),
                new Counter(30)
        );

        Optional<Counter> res = CounterStreams.findMax(counterList, 0, 0);
        Assert.assertFalse(res.isPresent());

        res = CounterStreams.findMax(counterList, 10, 0);
        Assert.assertFalse(res.isPresent());

        res = CounterStreams.findMax(counterList, 0, 10);
        Assert.assertTrue(res.isPresent());
        Assert.assertEquals(counterList.get(0), res.get());

        res = CounterStreams.findMax(counterList, 0, 26);
        Assert.assertTrue(res.isPresent());
        Assert.assertEquals(counterList.get(3), res.get());

        res = CounterStreams.findMax(counterList, 40, 50);
        Assert.assertFalse(res.isPresent());

        res = CounterStreams.findMax(counterList, -100, 40);
        Assert.assertTrue(res.isPresent());
        Assert.assertEquals(counterList.get(4), res.get());
    }

    @Test
    public void findAboveAvg() throws Exception {
        List<Counter> counterList = Arrays.asList(
                new Counter(10),
                new Counter(15),
                new Counter(20),
                new Counter(25),
                new Counter(30)
        );

        long res = CounterStreams.findAboveAvg(counterList);
        Assert.assertEquals(2, res);
    }

    @Test
    public void groupByValue() throws Exception {
        List<Counter> counterList = Arrays.asList(
                new Counter(10),
                new Counter(15),
                new Counter(20),
                new Counter(25),
                new Counter(30),

                new Counter(10),
                new Counter(15),
                new Counter(30)

        );

        Map<Integer, List<Counter>> res = CounterStreams.groupByValue(counterList);
        Assert.assertEquals(5, res.size());

        Assert.assertEquals(2, res.get(10).size());
        Assert.assertEquals(2, res.get(15).size());
        Assert.assertEquals(1, res.get(20).size());
        Assert.assertEquals(1, res.get(25).size());
        Assert.assertEquals(2, res.get(30).size());
    }

}