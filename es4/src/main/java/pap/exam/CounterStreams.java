package pap.exam;

import pap.exam.Counter;

import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * Created by thomas on 17/04/17.
 */
public class CounterStreams {

    /**
     * Data una lista di contatori, incrementa tutti quelli con valore pari a zero
     * @param counters Lista di contatori
     */
    public static void inc(List<Counter> counters) {
        counters.stream().filter(counter -> counter.getValue() == 0).forEach(counter -> counter.inc());
    }

    /**
     * Data una lista di contatori e due valori interi min e max, con min < max,
     * determina il contatore con il valore massimo fra quelli con valore compreso in [min,max]
     *
     * @param counters Lista di contatori
     * @param min Valore minimo
     * @param max Valore massimo
     * @return Il contatore con valore massimo nell'intervallo
     */
    public static Optional<Counter> findMax(List<Counter> counters, int min, int max) {
        return counters.stream()
                .filter(counter -> counter.getValue() >= min)
                .filter(counter -> counter.getValue() <= max)
                .max(Comparator.comparingInt(Counter::getValue));
    }

    /**
     * Data una lista di contatori conta il numero di quelli che hanno valore maggiore della media dei contatori
     *
     * @param counters Lista di contatori
     * @return Numero di contati con valori sopra la media
     */
    public static long findAboveAvg(List<Counter> counters) {
        double avg = counters.stream().mapToInt(Counter::getValue).average().getAsDouble();
        return counters.stream().filter(counter -> counter.getValue() > avg).count();
    }

    /**
     * Data una lista di contatori costruisce una mappa in cui si raggruppano tutti i contatori
     * che hanno il medesimo valore, utilizzato come chiave
     *
     * @param counters
     * @return La mappa di contatatori con lo stesso valore
     */
    public static Map<Integer, List<Counter>> groupByValue(List<Counter> counters) {
        return counters.stream().collect(Collectors.groupingBy(o -> o.getValue(), Collectors.toList()));
    }

}
