package pap.exam.thomas.multithreading;

import org.junit.Test;
import pap.exam.thomas.common.BodiesConstant;

/**
 * Created by thomas on 13/05/17.
 */
public class BodyActorCalculationImplTest {
    @Test
    public void calc() throws Exception {
        BodyActorCalculationImpl calculation = new BodyActorCalculationImpl();
        calculation.calc(BodiesConstant.BP_EARTH_2MOON().getBodyPositions(), 1);
    }

}
