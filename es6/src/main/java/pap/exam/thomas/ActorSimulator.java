package pap.exam.thomas;

import akka.actor.ActorSystem;
import pap.exam.thomas.multithreading.BodyActorCalculationImpl;

import static akka.pattern.Patterns.ask;

/**
 * Created by thomas on 26/04/17.
 */
public class ActorSimulator extends Simulator {

    public ActorSimulator() {
        super(new BodyActorCalculationImpl());
    }

    @Override
    public void onStart() {
        super.onStart();
    }
}
