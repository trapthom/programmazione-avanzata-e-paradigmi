package pap.exam.thomas.multithreading;

import akka.actor.ActorRef;
import akka.actor.ActorSystem;
import akka.actor.Props;
import pap.exam.thomas.entitites.Body;
import pap.exam.thomas.entitites.DoubleBidimentionalVector;
import pap.exam.thomas.multithreading.actor.*;
import scala.concurrent.AwaitPermission$;
import scala.concurrent.Future;
import scala.concurrent.duration.Duration;

import java.util.ArrayList;
import java.util.List;

import static akka.pattern.Patterns.ask;

/**
 * Created by thomas on 26/04/17.
 */
public class BodyActorCalculationImpl implements BodyCalculation {
    final ActorSystem system = ActorSystem.create("ActorSimulator");

    @Override
    public void calc(List<Body> bodies, int tick) throws BodyCalculationException, BodiesForceCalculationException {
        DoubleBidimentionalVector[][] forceMatrix = computeForceMatrix(bodies);
        updateBodiesPositions(bodies, forceMatrix, tick);
    }

    private void updateBodiesPositions(List<Body> bodies, DoubleBidimentionalVector[][] forceMatrix, int tick) throws BodyCalculationException {
        ActorRef res = system.actorOf(Props.create(MainBodyActor.class));

        final ArrayList<Future<Object>> futureArrayList = new ArrayList<>();
        Future<Object> f1 = ask(res, new MainBodyRequest(bodies, forceMatrix, tick), Integer.MAX_VALUE);
        futureArrayList.add(f1);

        MainBodyResponse response;
        try {
            response = (MainBodyResponse) f1.result(Duration.Inf(), null);
        } catch (Exception e) {
            throw new BodyCalculationException(e);
        }
        if (response == null || !response.isCompleted())
            throw new BodyCalculationException("Invalid response from actors");

    }

    private DoubleBidimentionalVector[][] computeForceMatrix(List<Body> bodies) throws BodiesForceCalculationException {
        ActorRef res = system.actorOf(Props.create(MainBodiesForceActor.class));

        final ArrayList<Future<Object>> futureArrayList = new ArrayList<>();
        Future<Object> f1 = ask(res, new MainBodiesForceRequest(bodies), Integer.MAX_VALUE);
        futureArrayList.add(f1);

        MainBodiesForceResponse response = null;
        try {
            response = (MainBodiesForceResponse) f1.result(Duration.Inf(), null);
        } catch (Exception e) {
            throw new BodiesForceCalculationException(e);
        }
        return response.getVectors();
    }

}
