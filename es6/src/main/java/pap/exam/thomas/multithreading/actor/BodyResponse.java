package pap.exam.thomas.multithreading.actor;

import pap.exam.thomas.entitites.Body;
import pap.exam.thomas.entitites.DoubleBidimentionalVector;

/**
 * Created by thomas on 26/04/17.
 */
public class BodyResponse {

    private final boolean completed;

    public BodyResponse(boolean completed) {
        this.completed = completed;
    }

    public boolean isCompleted() {
        return completed;
    }
}