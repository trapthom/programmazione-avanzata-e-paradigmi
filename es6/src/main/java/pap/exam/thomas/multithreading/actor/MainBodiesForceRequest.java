package pap.exam.thomas.multithreading.actor;

import pap.exam.thomas.entitites.Body;

import java.util.List;

/**
 * Created by thomas on 26/04/17.
 */
public class MainBodiesForceRequest {
    private final List<Body> bodies;

    public MainBodiesForceRequest(List<Body> bodies) {
        this.bodies = bodies;
    }

    public List<Body> getBodies() {
        return bodies;
    }
}