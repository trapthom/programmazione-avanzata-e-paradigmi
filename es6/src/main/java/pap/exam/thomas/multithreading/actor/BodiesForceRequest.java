package pap.exam.thomas.multithreading.actor;

import pap.exam.thomas.entitites.Body;
import pap.exam.thomas.multithreading.common.TypedActor;

/**
 * Created by thomas on 26/04/17.
 */
public class BodiesForceRequest {
    private final Body b1, b2;

    public BodiesForceRequest(Body b1, Body b2) {
        this.b1 = b1;
        this.b2 = b2;
    }

    public Body getB1() {
        return b1;
    }

    public Body getB2() {
        return b2;
    }

}