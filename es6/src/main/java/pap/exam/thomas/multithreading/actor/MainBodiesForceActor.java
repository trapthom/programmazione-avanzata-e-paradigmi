package pap.exam.thomas.multithreading.actor;

import akka.actor.ActorRef;
import akka.actor.Props;
import akka.actor.UntypedActor;
import akka.event.Logging;
import akka.event.LoggingAdapter;
import pap.exam.thomas.entitites.Body;
import pap.exam.thomas.entitites.DoubleBidimentionalVector;

import java.util.List;

/**
 * Created by thomas on 26/04/17.
 */
public class MainBodiesForceActor extends UntypedActor {

    LoggingAdapter log = Logging.getLogger(getContext().system(), this);
    private ActorRef sender;
    private int count = 0, returned = 0, started = 0;
    private DoubleBidimentionalVector[][] result;

    @Override
    public void onReceive(Object o) throws Throwable {
        log.debug("Received String message: {}", o);
        if (o instanceof MainBodiesForceRequest) {
            onReceiveMessage((MainBodiesForceRequest) o);
        } else if (o instanceof BodiesForceResponse) {
            onReceiveMessage((BodiesForceResponse) o);
        }
    }

    public void onReceiveMessage(MainBodiesForceRequest message) {
        //Inizializzo i campi della classe che conterranno il risultato
        List<Body> bodies = message.getBodies();
        this.sender = getSender();
        this.count = bodies.size() * (bodies.size() + 1) / 2;
        this.returned = 0;

        this.result = new DoubleBidimentionalVector[bodies.size()][];
        for (int x = 0; x < this.result.length; x++) {
            this.result[x] = new DoubleBidimentionalVector[bodies.size()];
        }


        //Invio la richiesta di calcolo agli attori
        ActorRef res = getContext().actorOf(Props.create(BodiesForceActor.class));
        for (int y = 0; y < this.result.length; y++) {
            for (int x = y; x < this.result.length; x++) {
                started++;
                res.tell(new BodiesForceRequest(bodies.get(x), bodies.get(y)), getSelf());
            }
        }
    }

    public void onReceiveMessage(BodiesForceResponse message) {
        this.result[message.getIdBody1()][message.getIdBody2()] = message.getVector();
        this.result[message.getIdBody2()][message.getIdBody1()] = negate(message.getVector());
        returned++;
        if (count == returned)
            this.sender.tell(new MainBodiesForceResponse(this.result), getSelf());
    }

    private static DoubleBidimentionalVector negate(DoubleBidimentionalVector v) {
        return new DoubleBidimentionalVector(-v.getX(), -v.getY());
    }
}