package pap.exam.thomas.multithreading.common;

import akka.actor.InvalidMessageException;
import akka.actor.UntypedActor;

/**
 * Created by thomas on 26/04/17.
 */
public abstract class TypedActor<T> extends UntypedActor {

    @Override
    public void onReceive(Object o) throws Throwable {
        T message = null;
        try {
            message = (T) o;
        } catch (Exception e) {
            unhandled(message);
            return;
        }
        onReceiveMessage(message);
    }

    public abstract void onReceiveMessage(T message);
}
