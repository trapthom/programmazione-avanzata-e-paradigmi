package pap.exam.thomas.multithreading.actor;

import akka.actor.UntypedActor;
import akka.event.Logging;
import akka.event.LoggingAdapter;
import pap.exam.thomas.entitites.Body;
import pap.exam.thomas.entitites.DoubleBidimentionalVector;

/**
 * Created by thomas on 26/04/17.
 */
public class BodyRequest {

    private final Body body;
    private final DoubleBidimentionalVector[][] forceMatrix;
    private int tick;

    public BodyRequest(Body body, DoubleBidimentionalVector[][] forceMatrix, int tick) {
        this.body = body;
        this.forceMatrix = forceMatrix;
        this.tick = tick;
    }

    public Body getBody() {
        return body;
    }

    public DoubleBidimentionalVector[][] getForceMatrix() {
        return forceMatrix;
    }

    public int getTick() {
        return tick;
    }

    public void setTick(int tick) {
        this.tick = tick;
    }
}