package pap.exam.thomas.multithreading.actor;

import akka.event.Logging;
import akka.event.LoggingAdapter;
import pap.exam.thomas.common.PhysicsUtils;
import pap.exam.thomas.entitites.DoubleBidimentionalVector;
import pap.exam.thomas.multithreading.common.TypedActor;

/**
 * Created by thomas on 26/04/17.
 */
public class BodiesForceActor extends TypedActor<BodiesForceRequest> {
    LoggingAdapter log = Logging.getLogger(getContext().system(), this);

    @Override
    public void onReceiveMessage(BodiesForceRequest message) {
        log.debug("Received String message: {}", message);
        DoubleBidimentionalVector tmp = PhysicsUtils.computeForce(message.getB1(), message.getB2());
        BodiesForceResponse response = new BodiesForceResponse(tmp, message.getB1().getId(), message.getB2().getId());
        getSender().tell(response, getSelf());
    }
}