package pap.exam.thomas.multithreading.actor;

import pap.exam.thomas.entitites.Body;
import pap.exam.thomas.entitites.DoubleBidimentionalVector;

import java.util.List;

/**
 * Created by thomas on 26/04/17.
 */
public class MainBodiesForceResponse {
    private final DoubleBidimentionalVector[][] vectors;

    public MainBodiesForceResponse(DoubleBidimentionalVector[][] vectors) {
        this.vectors = vectors;
    }

    public DoubleBidimentionalVector[][] getVectors() {
        return vectors;
    }
}