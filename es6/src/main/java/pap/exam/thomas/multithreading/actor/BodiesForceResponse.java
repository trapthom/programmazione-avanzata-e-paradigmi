package pap.exam.thomas.multithreading.actor;

import pap.exam.thomas.entitites.DoubleBidimentionalVector;

/**
 * Created by thomas on 26/04/17.
 */
public class BodiesForceResponse {
    private final DoubleBidimentionalVector vector;
    private final int idBody1;
    private final int idBody2;

    public BodiesForceResponse(DoubleBidimentionalVector vector, int idBody1, int idBody2) {
        this.vector = vector;
        this.idBody1 = idBody1;
        this.idBody2 = idBody2;
    }

    public DoubleBidimentionalVector getVector() {
        return this.vector;
    }

    public int getIdBody1() {
        return this.idBody1;
    }

    public int getIdBody2() {
        return this.idBody2;
    }

}