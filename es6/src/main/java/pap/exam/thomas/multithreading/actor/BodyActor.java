package pap.exam.thomas.multithreading.actor;

import akka.event.Logging;
import akka.event.LoggingAdapter;
import pap.exam.thomas.entitites.DoubleBidimentionalVector;
import pap.exam.thomas.multithreading.common.TypedActor;

import java.util.Arrays;
import java.util.Optional;

/**
 * Created by thomas on 26/04/17.
 */
public class BodyActor extends TypedActor<BodyRequest> {
    LoggingAdapter log = Logging.getLogger(getContext().system(), this);

    @Override
    public void onReceiveMessage(BodyRequest message) {
        log.debug("Received String message: {}", message);

        Optional<DoubleBidimentionalVector> f = Arrays.stream(message.getForceMatrix()[message.getBody().getId()])
                .reduce((v1, v2) -> new DoubleBidimentionalVector(v1.getX() + v2.getX(), v1.getY() + v2.getY()));
        double ax = f.get().getX() / message.getBody().getMass();
        double ay = f.get().getY() / message.getBody().getMass();
        double vx = message.getBody().getVx() + ax * (double) message.getTick();
        double vy = message.getBody().getVy() + ay * (double) message.getTick();
        message.getBody().setVx(vx);
        message.getBody().setVy(vy);
        double x = message.getBody().getX() + message.getBody().getVx() * (double) message.getTick();
        double y = message.getBody().getY() + message.getBody().getVy() * (double) message.getTick();
        if (message.getBody().getId() == 1) {
            System.out.println("[DX:" + (x - message.getBody().getX()) + "][DY:" + (y - message.getBody().getY()) + "]");
            System.out.println("[F:" + f.get().getModule() + "]");
        }

        message.getBody().setX(x);
        message.getBody().setY(y);
        getSender().tell(new BodyResponse(true), getSelf());
    }
}