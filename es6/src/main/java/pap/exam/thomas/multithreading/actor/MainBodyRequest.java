package pap.exam.thomas.multithreading.actor;

import pap.exam.thomas.entitites.Body;
import pap.exam.thomas.entitites.DoubleBidimentionalVector;

import java.util.List;

/**
 * Created by thomas on 26/04/17.
 */
public class MainBodyRequest {

    private final List<Body> bodies;
    private final DoubleBidimentionalVector[][] forceMatrix;
    private final int tick;

    public MainBodyRequest(List<Body> bodies, DoubleBidimentionalVector[][] forceMatrix, int tick) {
        this.bodies = bodies;
        this.forceMatrix = forceMatrix;
        this.tick = tick;
    }

    public List<Body> getBodies() {
        return bodies;
    }

    public DoubleBidimentionalVector[][] getForceMatrix() {
        return forceMatrix;
    }

    public int getTick() {
        return tick;
    }
}
