package pap.exam.thomas.multithreading.actor;

import akka.actor.ActorRef;
import akka.actor.Props;
import akka.actor.UntypedActor;
import akka.event.Logging;
import akka.event.LoggingAdapter;
import pap.exam.thomas.entitites.Body;

import java.util.List;

/**
 * Created by thomas on 26/04/17.
 */
public class MainBodyActor extends UntypedActor {

    LoggingAdapter log = Logging.getLogger(getContext().system(), this);
    private ActorRef sender;
    private int count, returned;

    @Override
    public void onReceive(Object o) throws Throwable {
        log.debug("Received String message: {}", o);
        if (o instanceof MainBodyRequest) {
            onReceiveMessage((MainBodyRequest) o);
        } else if (o instanceof BodyResponse) {
            onReceiveMessage((BodyResponse) o);
        }
    }

    public void onReceiveMessage(MainBodyRequest message) {
        //Inizializzo i campi della classe che conterranno il risultato
        List<Body> bodies = message.getBodies();
        this.sender = getSender();
        this.count = bodies.size();
        this.returned = 0;

        //Invio la richiesta di calcolo agli attori
        ActorRef res = getContext().actorOf(Props.create(BodyActor.class));
        for (Body b : bodies) {
            res.tell(new BodyRequest(b, message.getForceMatrix(), message.getTick()), getSelf());
        }
    }

    public void onReceiveMessage(BodyResponse message) {
        returned++;
        if (count == returned)
            this.sender.tell(new MainBodyResponse(true), getSelf());
    }

}