package pap.exam.thomas.gui;

import javax.swing.*;
import javax.swing.text.NumberFormatter;
import java.awt.*;

/**
 * Created by thomas on 17/04/17.
 */
class GuiRenderer {

    private final GuiItems items;

    public GuiRenderer() {
        items = new GuiItems(
                new JButton("Start"),
                new JButton("Stop"),
                new JButton("Pause"),
                new JLabel("-"),
                new JLabel("-"),
                new JLabel("-"),
                new JSpinner(),
                new JSpinner(),
                new JTextPane());
    }


    public void show() {
        JFrame mainFrame = createMainFrame();
        JPanel buttonPanel = createButtonPanel();
        JPanel infoPanel = createInfoPanel();
        JPanel controlPanel = createControlPanel();
        composeMainFrame(mainFrame, infoPanel, buttonPanel, controlPanel);
        mainFrame.setExtendedState(JFrame.MAXIMIZED_BOTH);
        mainFrame.setVisible(true);
    }

    private JPanel createInfoPanel() {
        Font font = items.getLblAvg().getFont();
        items.getLblAvg().setFont(new Font(font.getName(), Font.PLAIN, 60));
        items.getLblMin().setFont(new Font(font.getName(), Font.PLAIN, 60));
        items.getLblMax().setFont(new Font(font.getName(), Font.PLAIN, 60));


        JLabel lbl = new JLabel("AVG: ");
        lbl.setFont(new Font(font.getName(), Font.PLAIN, 60));

        JPanel avgPanel = new JPanel();
        avgPanel.setLayout(new FlowLayout(FlowLayout.CENTER));
        avgPanel.add(lbl);
        avgPanel.add(items.getLblAvg());


        lbl = new JLabel("MIN: ");
        lbl.setFont(new Font(font.getName(), Font.PLAIN, 60));

        JPanel minPanel = new JPanel();
        minPanel.setLayout(new FlowLayout(FlowLayout.CENTER));
        minPanel.add(lbl);
        minPanel.add(items.getLblMin());

        lbl = new JLabel("MAX: ");
        lbl.setFont(new Font(font.getName(), Font.PLAIN, 60));

        JPanel maxPanel = new JPanel();
        maxPanel.setLayout(new FlowLayout(FlowLayout.CENTER));
        maxPanel.add(lbl);
        maxPanel.add(items.getLblMax());


        return new JLinearLayout()
                .setChildOrientation(JLinearLayout.Orientation.VERTICAL)
                .addView(avgPanel, 0.1)
                .addView(minPanel, 0.1)
                .addView(maxPanel, 0.1)
                .getAsPanel();
    }

    private JPanel createControlPanel() {
        JPanel tolerancePanel = new JPanel();
        tolerancePanel.add(new JLabel("Tolerance Value: "));
        items.getSpnTolerance().setModel(new SpinnerNumberModel(10, 0, 1000, 0.5));
        JFormattedTextField txt = ((JSpinner.NumberEditor) items.getSpnTolerance().getEditor()).getTextField();
        ((NumberFormatter) txt.getFormatter()).setAllowsInvalid(false);
        tolerancePanel.add(items.getSpnTolerance());

        JPanel toleranceTimePanel = new JPanel();
        toleranceTimePanel.add(new JLabel("Tolerance Time: "));
        items.getSpnToleranceMillisecond().setModel(new SpinnerNumberModel(10 * 1000, 1, 1000 * 1000, 1));
        txt = ((JSpinner.NumberEditor) items.getSpnTolerance().getEditor()).getTextField();
        ((NumberFormatter) txt.getFormatter()).setAllowsInvalid(false);
        toleranceTimePanel.add(items.getSpnToleranceMillisecond());


        return new JLinearLayout()
                .setChildOrientation(JLinearLayout.Orientation.HORIZONTAL)
                .addView(tolerancePanel, 0.1)
                .addView(toleranceTimePanel, 0.1)
                .getAsPanel();
    }


    private void composeMainFrame(JFrame mainFrame, JPanel mainPanel, JPanel buttonPanel, JPanel controlPanel) {
        JEditorPane epMessage = items.getEpMessage();
        epMessage.setEditable(false);

        GridLayout layout = new GridLayout(1, 2);
        JPanel topPanel = new JPanel();
        topPanel.setLayout(layout);

        topPanel.add(mainPanel);
        topPanel.add(new JScrollPane(epMessage));

        JPanel content = new JLinearLayout()
                .setChildOrientation(JLinearLayout.Orientation.VERTICAL)
                .addView(topPanel, 0.9D)
                .addView(controlPanel, 0.1D)
                .addView(buttonPanel, 0.1D)
                .getAsPanel();


        mainFrame.getContentPane().add(content);
    }

    private JPanel createButtonPanel() {
        JPanel panel = new JPanel();
        panel.setLayout(new FlowLayout(FlowLayout.CENTER));

        panel.add(items.getBtnStart());
        panel.add(items.getBtnStop());
        panel.add(items.getBtnPause());

        return panel;
    }

    private static JFrame createMainFrame() {
        // Creating instance of JFrame
        JFrame frame = new JFrame("Temperature control center");
        // Setting the width and height of frame
        frame.setSize(500, 300);
        frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);

        return frame;
    }

    public GuiItems getItems() {
        return items;
    }
}
