package pap.exam.thomas.gui;

import javax.swing.*;

public class GuiItems {
    private final JButton btnStart;
    private final JButton btnStop;
    private final JButton btnPause;
    private final JLabel lblMin;
    private final JLabel lblMax;
    private final JLabel lblAvg;
    private final JSpinner spnTolerance;
    private final JSpinner spnToleranceMillisecond;
    private final JTextPane epMessage;

    public GuiItems(JButton btnStart, JButton btnStop, JButton btnPause,
                    JLabel lblMin, JLabel lblMax, JLabel lblAvg,
                    JSpinner spnTolerance, JSpinner spnToleranceMillisecond,
                    JTextPane epMessage) {
        this.btnStart = btnStart;
        this.btnStop = btnStop;
        this.btnPause = btnPause;
        this.lblMin = lblMin;
        this.lblMax = lblMax;
        this.lblAvg = lblAvg;
        this.spnTolerance = spnTolerance;
        this.spnToleranceMillisecond = spnToleranceMillisecond;
        this.epMessage = epMessage;
    }

    public JButton getBtnStart() {
        return btnStart;
    }

    public JButton getBtnStop() {
        return btnStop;
    }

    public JButton getBtnPause() {
        return btnPause;
    }

    public JLabel getLblMin() {
        return lblMin;
    }

    public JLabel getLblMax() {
        return lblMax;
    }

    public JLabel getLblAvg() {
        return lblAvg;
    }

    public JSpinner getSpnTolerance() {
        return spnTolerance;
    }

    public JSpinner getSpnToleranceMillisecond() {
        return spnToleranceMillisecond;
    }

    public JTextPane getEpMessage() {
        return epMessage;
    }
}