package pap.exam.thomas.gui;

/**
 * Created by thomas on 17/04/17.
 */
public class GuiManager {
    private static final GuiManager INSTANCE = new GuiManager();

    public static GuiManager get() {
        return INSTANCE;
    }

    private final GuiRenderer guiRender;

    private GuiManager() {
        guiRender = new GuiRenderer();
    }

    public void renderGui(GuiHandler guiHandler) {
        guiRender.show();
        guiHandler.applyHandler(guiRender.getItems());
    }

}
