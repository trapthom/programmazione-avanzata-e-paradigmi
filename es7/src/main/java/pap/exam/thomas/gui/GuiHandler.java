package pap.exam.thomas.gui;

/**
 * Created by thomas on 17/04/17.
 */
public interface GuiHandler {
    void applyHandler(GuiItems items);
}
