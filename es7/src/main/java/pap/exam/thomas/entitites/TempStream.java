package pap.exam.thomas.entitites;


import rx.Observable;

import java.util.concurrent.TimeUnit;

/**
 * Created by thomas on 26/04/17.
 */
class TempStream {

    public static Observable<Double> create(long millisecond) {
        TempSensor sens = new TempSensor(1, 100, 0.1);

        return Observable.interval(millisecond, TimeUnit.MILLISECONDS)
                .map(longTimeInterval -> sens.getCurrentValue());
    }

}
