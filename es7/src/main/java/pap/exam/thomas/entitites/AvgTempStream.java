package pap.exam.thomas.entitites;


import rx.Observable;
import rx.Subscription;
import rx.functions.Action1;

import java.util.List;
import java.util.concurrent.TimeUnit;

/**
 * Created by thomas on 26/04/17.
 */
public class AvgTempStream {
    private double maxVariation;
    private Observable<Double> s1, s2, s3;
    private Observable<Double> avgStream;
    private Subscription alertSubscription, avgSubscription;

    private double min = Double.MAX_VALUE;
    private double max = -Double.MAX_VALUE;

    public AvgTempStream(double maxVariation) {
        this(1000, maxVariation);
    }

    private AvgTempStream(int frequency, double maxVariation) {
        this.maxVariation = maxVariation;
        s1 = filter(TempStream.create(frequency), maxVariation);
        s2 = filter(TempStream.create(frequency), maxVariation);
        s3 = filter(TempStream.create(frequency), maxVariation);
    }

    private void initStream() {
        avgStream = Observable.zip(s1, s2, s3, (aDouble, aDouble2, aDouble3) -> {
            double avg = (aDouble + aDouble2 + aDouble3) / 3;
            System.out.println("AVG: (" + aDouble + " + " + aDouble2 + " + " + aDouble3 + ") / 3 = " + avg);
            return avg;
        });
    }

    static Observable<Double> filter(Observable<Double> s, double maxVariation) {
        return s.scan((aDouble, aDouble2) -> {
            if (Math.abs(aDouble - aDouble2) < maxVariation)
                return aDouble2;

            System.out.println("Excluded " + aDouble2 + ", replaced with previous value " + aDouble);
            return aDouble;
        });
    }

    public void subscribeAlert(double alertThreshold, int alertThresholdTime, final Action1<? super Double> onNext) {
        if (avgStream == null) initStream();
        if (alertSubscription != null) alertSubscription.unsubscribe();
        alertSubscription = avgStream.buffer(alertThresholdTime, TimeUnit.MILLISECONDS)
                .map(doubles -> max(doubles, alertThreshold, Double.MIN_VALUE))
                .filter(m -> m > Double.MIN_VALUE)
                .subscribe(onNext);
    }

    public void subscribeAverage(TempAction tempAction) {
        if (avgStream == null) initStream();
        if (avgSubscription != null) avgSubscription.unsubscribe();
        avgSubscription = avgStream.subscribe(avg -> {
            min = Math.min(min, avg);
            max = Math.max(max, avg);
            tempAction.onTempUpdate(avg, min, max);
        });
    }

    private static double max(List<Double> doubles, double alertThreshold, double defaultValue) {
        if (doubles == null || doubles.size() == 0) return defaultValue;
        double max = Double.MIN_VALUE;
        for (Double d : doubles) {
            if (d < alertThreshold) return defaultValue;
            max = Math.max(max, d);

        }
        return max;
    }

    public void unsubscribeAll() {
        if (alertSubscription != null) alertSubscription.unsubscribe();
        if (avgSubscription != null) avgSubscription.unsubscribe();
    }

    public interface TempAction {
        void onTempUpdate(double avg, double min, double max);
    }

    @Override
    protected void finalize() throws Throwable {
        unsubscribeAll();
    }
}
