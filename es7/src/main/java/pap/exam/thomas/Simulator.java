package pap.exam.thomas;

import pap.exam.thomas.entitites.AvgTempStream;
import pap.exam.thomas.gui.GuiHandler;
import pap.exam.thomas.gui.GuiItems;
import pap.exam.thomas.gui.GuiManager;

import javax.swing.*;
import javax.swing.text.BadLocationException;
import javax.swing.text.SimpleAttributeSet;
import javax.swing.text.StyleConstants;
import javax.swing.text.StyledDocument;
import java.awt.*;
import java.text.DecimalFormat;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;

/**
 * Created by thomas on 22/04/17.
 */
public class Simulator implements GuiHandler {
    public static final Double MAX_VARIATION = 30D;

    private double alertThreshold = 70;
    private int alertThresholdTime = 1000;

    private AvgTempStream avgTempStream;

    private final DecimalFormat formatter = new DecimalFormat("0.## °C");

    public void startGui() {
        GuiManager.get().renderGui(this);
    }

    public void applyHandler(GuiItems items) {
        items.getSpnTolerance().setValue(alertThreshold);
        items.getSpnToleranceMillisecond().setValue(alertThresholdTime);

        items.getBtnStart().addActionListener(e -> {
            if (avgTempStream == null)
                avgTempStream = new AvgTempStream(MAX_VARIATION);
            updateUI(items);
            updateThreshold(items);
            addUserLog(items, "Simulation started", Color.BLUE);
        });

        items.getBtnStop().addActionListener(e -> {
            addUserLog(items, "Simulation stopped", Color.BLUE);
            avgTempStream.unsubscribeAll();
            avgTempStream = null;
        });

        items.getBtnPause().addActionListener(e -> {
            addUserLog(items, "Simulation paused", Color.BLUE);
            avgTempStream.unsubscribeAll();
        });

        items.getSpnTolerance().addChangeListener(e -> {
            JSpinner s = (JSpinner) e.getSource();
            Double tmp = Double.valueOf(s.getValue().toString());
            addUserLog(items, "Alert threshold updated: " + formatter.format(alertThreshold) + " -> " + formatter.format(tmp), Color.BLACK);
            alertThreshold = tmp;
            updateThreshold(items);
        });

        items.getSpnToleranceMillisecond().addChangeListener(e -> {
            JSpinner s = (JSpinner) e.getSource();
            Integer tmp = Integer.valueOf(s.getValue().toString());
            addUserLog(items, "Alert threshold time updated: " + alertThresholdTime + " -> " + tmp, Color.BLACK);
            alertThresholdTime = tmp;
            updateThreshold(items);
        });

    }

    private void updateUI(GuiItems items) {
        if (avgTempStream == null) return;
        avgTempStream.subscribeAverage((avg, min, max) -> {
            items.getLblAvg().setText(formatter.format(avg));
            items.getLblMin().setText(formatter.format(min));
            items.getLblMax().setText(formatter.format(max));
        });
    }

    private void updateThreshold(GuiItems items) {
        if (avgTempStream == null) return;

        avgTempStream.subscribeAlert(alertThreshold, alertThresholdTime,
                maxAvg -> addUserLog(items,
                        "Temperature greater than " + formatter.format(alertThreshold) + " for more than " + alertThresholdTime + "ms - MaxValue = " + formatter.format(maxAvg),
                        Color.RED));
    }

    private void addUserLog(GuiItems items, String message, Color color) {
        LocalTime time = LocalTime.now();
        String completeMessage = time.format(DateTimeFormatter.ofPattern("hh:mm:ss")) + ": " + message + "\n";

        JTextPane textPane = items.getEpMessage();
        StyledDocument doc = textPane.getStyledDocument();

        SimpleAttributeSet keyWord = new SimpleAttributeSet();
        StyleConstants.setForeground(keyWord, color);
        StyleConstants.setBold(keyWord, true);

        try {
            doc.insertString(doc.getLength(), completeMessage, keyWord);
        } catch (BadLocationException e) {
            e.printStackTrace();
        }

    }

}
