package pap.exam.thomas.entitites;

import org.junit.Assert;
import org.junit.Test;
import rx.Observable;

/**
 * Created by thomas on 27/04/17.
 */
public class AvgTempStreamTest {

    @Test
    public void filter() throws Exception {
        Observable<Double> s = Observable.just(2.0, 3.0, 4.0, 50.0, 3.0, 2.0, 4.0);
        Observable<Double> r = AvgTempStream.filter(s, 10);
        r.subscribe(v -> Assert.assertNotEquals(v, 50.0));
        r.subscribe(System.out::println);
    }

}
