Gli esercizi sono numerati come segue:

Esercizi su Programmazione Funzionale in Haskell 
Es 1 - funzione maxSubSeq 
Es 2 - funzione levelWithMaxDots
Es 3 - funzione printVariance

Esercizi su OOP + Programmazione Funzionale in Java
Es 4 - Classe CounterStreams

Esercizio Programmazione Multi-threaded
Es 5 - Problema N corpi

Esercizio Programmazione ad Attori
Es 6 - Problema N corpi con akka


Esercizio su Reactive Programming in RxJava
Es 7 - Temperature Monitoring