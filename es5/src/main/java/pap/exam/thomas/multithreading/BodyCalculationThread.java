package pap.exam.thomas.multithreading;

import pap.exam.thomas.entitites.Body;
import pap.exam.thomas.entitites.DoubleBidimentionalVector;

import java.util.Arrays;
import java.util.Optional;
import java.util.concurrent.Callable;

/**
 * Created by thomas on 22/04/17.
 */
public class BodyCalculationThread implements Callable<Boolean> {

    private final Body body;
    private final DoubleBidimentionalVector[][] forceMatrix;
    private int tick;

    public BodyCalculationThread(Body body, DoubleBidimentionalVector[][] forceMatrix, int tick) {

        this.body = body;
        this.forceMatrix = forceMatrix;
        this.tick = tick;
    }

    @Override
    public Boolean call() throws Exception {
        Optional<DoubleBidimentionalVector> f = Arrays.stream(forceMatrix[body.getId()]).reduce((v1, v2) -> new DoubleBidimentionalVector(v1.getX() + v2.getX(), v1.getY() + v2.getY()));

        //F = m*a --> a =F/m
        double ax = f.get().getX() / body.getMass();
        double ay = f.get().getY() / body.getMass();

        double vx = body.getVx() + ax * tick;
        double vy = body.getVy() + ay * tick;

        body.setVx(vx);
        body.setVy(vy);

        double x = body.getX() + body.getVx() * tick;
        double y = body.getY() + body.getVy() * tick;

        if (body.getId() == 1) {
            System.out.println("[DX:" + (x - body.getX()) + "][DY:" + (y - body.getY()) + "]");
            System.out.println("[F:" + f.get().getModule() + "]");
        }

        body.setX(x);
        body.setY(y);

        return true;
    }
}
