package pap.exam.thomas.multithreading;

import pap.exam.thomas.entitites.DoubleBidimentionalVector;

/**
 * Created by thomas on 26/04/17.
 */
public class BodiesForceThreadResult {
    private DoubleBidimentionalVector vector;
    private int idBody1;
    private int idBody2;

    public BodiesForceThreadResult(DoubleBidimentionalVector vector, int idBody1, int idBody2) {
        this.vector = vector;
        this.idBody1 = idBody1;
        this.idBody2 = idBody2;
    }

    public DoubleBidimentionalVector getVector() {
        return vector;
    }

    public void setVector(DoubleBidimentionalVector vector) {
        this.vector = vector;
    }

    public int getIdBody1() {
        return idBody1;
    }

    public void setIdBody1(int idBody1) {
        this.idBody1 = idBody1;
    }

    public int getIdBody2() {
        return idBody2;
    }

    public void setIdBody2(int idBody2) {
        this.idBody2 = idBody2;
    }
}
