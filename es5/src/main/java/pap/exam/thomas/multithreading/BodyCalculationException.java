package pap.exam.thomas.multithreading;

/**
 * Created by thomas on 22/04/17.
 */
public class BodyCalculationException extends Exception {
    public BodyCalculationException(String message) {
        super(message);
    }

    public BodyCalculationException(String message, Throwable cause) {
        super(message, cause);
    }

    public BodyCalculationException(Throwable cause) {
        super(cause);
    }
}
