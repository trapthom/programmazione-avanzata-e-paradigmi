package pap.exam.thomas.multithreading;

import pap.exam.thomas.common.PhysicsUtils;
import pap.exam.thomas.entitites.Body;
import pap.exam.thomas.entitites.DoubleBidimentionalVector;

import java.util.concurrent.Callable;

/**
 * Created by thomas on 22/04/17.
 */
public class BodiesForceThread implements Callable<BodiesForceThreadResult> {

    private final Body b1, b2;

    public BodiesForceThread(Body b1, Body b2) {
        this.b1 = b1;
        this.b2 = b2;
    }

    @Override
    public BodiesForceThreadResult call() throws Exception {
        DoubleBidimentionalVector tmp = PhysicsUtils.computeForce(b1, b2);
        return new BodiesForceThreadResult(tmp, b1.getId(), b2.getId());
    }
}
