package pap.exam.thomas.multithreading;

import pap.exam.thomas.entitites.Body;

import java.util.List;

/**
 * Created by thomas on 26/04/17.
 */
public interface BodyCalculation {
    void calc(List<Body> bodies, int tick) throws BodyCalculationException, BodiesForceCalculationException;
}
