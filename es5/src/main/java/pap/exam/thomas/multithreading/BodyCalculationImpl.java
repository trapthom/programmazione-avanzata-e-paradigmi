package pap.exam.thomas.multithreading;

import pap.exam.thomas.entitites.Body;
import pap.exam.thomas.entitites.DoubleBidimentionalVector;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.stream.Collectors;

/**
 * Created by thomas on 22/04/17.
 */
public class BodyCalculationImpl implements BodyCalculation {

    private static final ExecutorService executor = Executors.newCachedThreadPool();

    @Override
    public void calc(List<Body> bodies, int tick) throws BodyCalculationException, BodiesForceCalculationException {
        DoubleBidimentionalVector[][] forceMatrix = computeForceMatrix(bodies);
        updateBodiesPositions(bodies, tick, forceMatrix);
    }

    private static void updateBodiesPositions(List<Body> bodies, int tick, DoubleBidimentionalVector[][] forceMatrix) throws BodyCalculationException {
        List<Future<Boolean>> futures = bodies.stream().map(body -> new BodyCalculationThread(body, forceMatrix, tick)).map(executor::submit).collect(Collectors.toList());

        try {
            for (Future<Boolean> f : futures) {
                f.get();
            }
        } catch (InterruptedException | ExecutionException e) {
            executor.shutdownNow();
            throw new BodyCalculationException(e);
        }
    }

    private static DoubleBidimentionalVector[][] computeForceMatrix(List<Body> bodies) throws BodiesForceCalculationException {
        DoubleBidimentionalVector[][] forceMatrix = new DoubleBidimentionalVector[bodies.size()][];
        for (int x = 0; x < forceMatrix.length; x++) {
            forceMatrix[x] = new DoubleBidimentionalVector[bodies.size()];
        }

        List<Future<BodiesForceThreadResult>> forceFuture = new ArrayList<>();
        for (int y = 0; y < forceMatrix.length; y++) {
            for (int x = y; x < forceMatrix.length; x++) {
                BodiesForceThread tmp = new BodiesForceThread(bodies.get(x), bodies.get(y));
                forceFuture.add(executor.submit(tmp));
            }
        }

        try {
            for (Future<BodiesForceThreadResult> f : forceFuture) {
                BodiesForceThreadResult res = f.get();
                forceMatrix[res.getIdBody1()][res.getIdBody2()] = res.getVector();
                forceMatrix[res.getIdBody2()][res.getIdBody1()] = negate(res.getVector());
            }
        } catch (InterruptedException | ExecutionException e) {
            executor.shutdownNow();
            throw new BodiesForceCalculationException(e);
        }
        return forceMatrix;
    }


    private static DoubleBidimentionalVector negate(DoubleBidimentionalVector v) {
        return new DoubleBidimentionalVector(-v.getX(), -v.getY());
    }
}
