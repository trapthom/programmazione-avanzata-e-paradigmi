package pap.exam.thomas.multithreading;

/**
 * Created by thomas on 22/04/17.
 */
public class BodiesForceCalculationException extends Exception {
    public BodiesForceCalculationException(String message) {
        super(message);
    }

    public BodiesForceCalculationException(String message, Throwable cause) {
        super(message, cause);
    }

    public BodiesForceCalculationException(Throwable cause) {
        super(cause);
    }
}
