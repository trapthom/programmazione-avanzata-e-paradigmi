package pap.exam.thomas.gui;

import javax.swing.*;
import javax.swing.border.BevelBorder;
import javax.swing.table.DefaultTableModel;
import java.awt.*;

/**
 * Created by thomas on 17/04/17.
 */
class GuiRenderer {

    private final GuiItems items;

    public GuiRenderer() {
        items = new GuiItems(
                new JButton("Start"),
                new JButton("Stop"),
                new JButton("Pause"),
                new JButton("Zoom +"),
                new JButton("Zoom -"),
                new JButton("Speed +"),
                new JButton("Speed -"),
                new DrawablePanel(),
                new JTable(new DefaultTableModel(new Object[][]{}, new Object[]{"Info", "Value"})),
                new JComboBox<>());
    }


    public void show() {
        JFrame mainFrame = createMainFrame();
        loadDrawingArea();
        JPanel buttonPanel = createButtonPanel();

        JPanel infoPanel = createInfoPanel();
        composeMainFrame(mainFrame, items.getDrawingArea(), buttonPanel, infoPanel);
        mainFrame.setExtendedState(JFrame.MAXIMIZED_BOTH);
        // Setting the frame visibility to true
        mainFrame.setVisible(true);
    }

    private JPanel createInfoPanel() {
        JPanel optionPanel = new JPanel();
        optionPanel.add(items.getExampleCombobox());


        JPanel panel = new JLinearLayout()
                .setChildOrientation(JLinearLayout.Orientation.VERTICAL)
                .addView(new JScrollPane(items.getTable()), 0.4)
                .addView(optionPanel, 0.6)
                .getAsPanel();
        panel.setBackground(Color.blue);
        return panel;
    }


    private void composeMainFrame(JFrame mainFrame, JPanel mainPanel, JPanel buttonPanel, JPanel infoPanel) {
        JPanel topPanel = new JLinearLayout()
                .setChildOrientation(JLinearLayout.Orientation.HORIZONTAL)
                .addView(mainPanel, 0.9D)
                .addView(infoPanel, 0.1D)
                .getAsPanel();

        JPanel content = new JLinearLayout()
                .setChildOrientation(JLinearLayout.Orientation.VERTICAL)
                .addView(topPanel, 0.9D)
                .addView(buttonPanel, 0.1D)
                .getAsPanel();

        mainFrame.getContentPane().add(content);
    }

    private JPanel createButtonPanel() {
        JPanel panel = new JPanel();
        panel.setLayout(new FlowLayout(FlowLayout.CENTER));

        panel.add(items.getBtnStart());
        panel.add(items.getBtnStop());
        panel.add(items.getBtnPause());
        panel.add(items.getBtnZoomIn());
        panel.add(items.getBtnZoomOut());

        panel.add(items.getBtnSpeedUp());
        panel.add(items.getBtnSpeedDown());

        return panel;
    }

    private void loadDrawingArea() {
        items.getDrawingArea().setLayout(new FlowLayout(FlowLayout.CENTER));
        items.getDrawingArea().setBackground(Color.WHITE);
        items.getDrawingArea().setBorder(BorderFactory.createBevelBorder(BevelBorder.RAISED));
    }

    private static JFrame createMainFrame() {
        // Creating instance of JFrame
        JFrame frame = new JFrame("Interplanetary control center");
        // Setting the width and height of frame
        frame.setSize(500, 500);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        return frame;
    }

    public GuiItems getItems() {
        return items;
    }
}
