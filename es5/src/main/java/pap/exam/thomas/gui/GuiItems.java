package pap.exam.thomas.gui;

import javax.swing.*;

public class GuiItems {
    private final JButton btnStart;
    private final JButton btnStop;
    private final JButton btnPause;
    private final JButton btnZoomIn;
    private final JButton btnZoomOut;
    private final JButton btnSpeedUp;
    private final JButton btnSpeedDown;
    private final DrawablePanel drawingArea;
    private final JTable table;
    private final JComboBox<String> exampleCombobox;

    public GuiItems(JButton btnStart, JButton btnStop, JButton btnPause,
                    JButton btnZoomIn, JButton btnZoomOut,
                    JButton btnSpeedUp, JButton btnSpeedDown,
                    DrawablePanel drawingArea,
                    JTable table,
                    JComboBox<String> exampleCombobox
                    ) {
        this.btnStart = btnStart;
        this.btnStop = btnStop;
        this.btnPause = btnPause;
        this.btnZoomIn = btnZoomIn;
        this.btnZoomOut = btnZoomOut;
        this.btnSpeedUp = btnSpeedUp;
        this.btnSpeedDown = btnSpeedDown;
        this.drawingArea = drawingArea;
        this.exampleCombobox = exampleCombobox;
        this.table = table;
    }

    public JButton getBtnStart() {
        return btnStart;
    }

    public JButton getBtnStop() {
        return btnStop;
    }

    public JButton getBtnPause() {
        return btnPause;
    }

    public JButton getBtnZoomIn() {
        return btnZoomIn;
    }

    public JButton getBtnZoomOut() {
        return btnZoomOut;
    }

    public JButton getBtnSpeedUp() {
        return btnSpeedUp;
    }

    public JButton getBtnSpeedDown() {
        return btnSpeedDown;
    }

    public DrawablePanel getDrawingArea() {
        return drawingArea;
    }

    public JTable getTable() {
        return table;
    }

    public JComboBox<String> getExampleCombobox() {
        return exampleCombobox;
    }
}