package pap.exam.thomas.gui;

/**
 * Created by thomas on 17/04/17.
 */
public class GuiManager {
    private static GuiManager INSTANCE = new GuiManager();

    public static GuiManager get() {
        return INSTANCE;
    }

    private GuiRenderer guiRender;

    private GuiManager() {
        guiRender = new GuiRenderer();
    }

    public void renderGui(GuiHandler guiHandler) {
        guiHandler.applyHandler(guiRender.getItems());
        guiRender.show();
    }

}
