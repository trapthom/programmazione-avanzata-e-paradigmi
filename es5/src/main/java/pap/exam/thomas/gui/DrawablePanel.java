package pap.exam.thomas.gui;

import javax.swing.*;
import java.awt.*;

/**
 * Created by thomas on 20/04/17.
 */
public class DrawablePanel extends JPanel {
    private GraphicsDraw graphicsDraw = null;

    @Override
    public void paint(Graphics g) {
        super.paint(g);
    }

    @Override
    public void paintComponent(Graphics g) {
        super.paintComponent(g);
        if (graphicsDraw != null) {
            graphicsDraw.draw(g, getWidth(), getHeight());
        }
    }

    public interface GraphicsDraw {
        void draw(Graphics g, int w, int h);
    }

    public GraphicsDraw getGraphicsDraw() {
        return graphicsDraw;
    }

    public void setGraphicsDraw(GraphicsDraw graphicsDraw) {
        this.graphicsDraw = graphicsDraw;
    }
}