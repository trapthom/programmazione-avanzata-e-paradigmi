package pap.exam.thomas.entitites;

/**
 * Created by thomas on 20/04/17.
 */
public class DoubleBidimentionalVector extends BidimentionalVector<Double> {

    public DoubleBidimentionalVector(double x, double y) {
        super(x, y);
    }


    public double getModule() {
        return Math.sqrt(Math.pow(getX(), 2) + Math.pow(getY(), 2));
    }
}
