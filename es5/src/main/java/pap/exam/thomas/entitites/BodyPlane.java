package pap.exam.thomas.entitites;

import java.util.List;
import java.util.Map;

/**
 * Created by thomas on 20/04/17.
 */
public class BodyPlane {
    private int width;
    private int height;

    private List<Body> bodyPositions;

    public BodyPlane(int width, int height, List<Body> bodyPositions) {
        this.width = width;
        this.height = height;
        this.bodyPositions = bodyPositions;
    }

    public int getWidth() {
        return width;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    public List<Body> getBodyPositions() {
        return bodyPositions;
    }

    public void setBodyPositions(List<Body> bodyPositions) {
        this.bodyPositions = bodyPositions;
    }
}
