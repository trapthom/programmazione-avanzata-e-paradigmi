package pap.exam.thomas.entitites;

/**
 * Created by thomas on 20/04/17.
 */
public class BidimentionalVector<T> {
    T x, y;

    public BidimentionalVector(T x, T y) {
        this.x = x;
        this.y = y;
    }

    public T getX() {
        return x;
    }

    public void setX(T x) {
        this.x = x;
    }

    public T getY() {
        return y;
    }

    public void setY(T y) {
        this.y = y;
    }


}
