package pap.exam.thomas.entitites;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by thomas on 20/04/17.
 */
public class BodyPlaneBuilder {
    private int w, h;
    private List<Body> bodies;

    public BodyPlaneBuilder() {
        bodies = new ArrayList<>();
    }

    public BodyPlane build() {
        return new BodyPlane(w, h, bodies);
    }


    public BodyPlaneBuilder width(int w) {
        this.w = w;
        return this;
    }

    public BodyPlaneBuilder height(int h) {
        this.h = h;
        return this;
    }

    public BodyPlaneBuilder addBody(double mass, double x, double y, double vx, double vy) {
        bodies.add(new Body(bodies.size(), mass, x, y, vx, vy));
        return this;
    }


}
