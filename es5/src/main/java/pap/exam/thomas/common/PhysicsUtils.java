package pap.exam.thomas.common;

import pap.exam.thomas.entitites.Body;
import pap.exam.thomas.entitites.DoubleBidimentionalVector;

import java.util.List;

/**
 * Created by thomas on 20/04/17.
 */
public class PhysicsUtils {
    //Gravitational constant
    public static final double G = 6.674 * Math.pow(10, -11);
    static final double MIN_DIST = 1 * Math.pow(10, 5);

    public static DoubleBidimentionalVector computeForce(Body b1, Body b2) {
        if (b1 == b2) return new DoubleBidimentionalVector(0, 0);
        //F = mi * mj * G / r2

        double rx = b2.getX() - b1.getX();
        double ry = b2.getY() - b1.getY();
        double alfa = Math.atan(ry / rx);


        double r = computeDist(b1.getX(), b2.getX(), b1.getY(), b2.getY());


        double f = b1.getMass() * b2.getMass() * G / Math.pow(r, 2);

        double fx = f * Math.cos(alfa);
        double fy = f * Math.sin(alfa);


        fx = Math.abs(fx) * Math.signum(rx);
        fy = Math.abs(fy) * Math.signum(ry);

        return new DoubleBidimentionalVector(fx, fy);
    }

    static double computeDist(double x1, double x2, double y1, double y2) {
        double ret = Math.sqrt(Math.pow((y2 - y1), 2) + Math.pow((x2 - x1), 2));
        if (ret < MIN_DIST)
            return MIN_DIST;
        return ret;
    }

}
