package pap.exam.thomas.common;

import java.awt.*;
import java.awt.font.FontRenderContext;
import java.awt.geom.Rectangle2D;

/**
 * Created by thomas on 22/04/17.
 */
public class Utils {

    /**
     * This method centers a <code>String</code> in
     * a bounding <code>Rectangle</code>.
     *
     * @param g    - The <code>Graphics</code> instance.
     * @param r    - The bounding <code>Rectangle</code>.
     * @param s    - The <code>String</code> to center in the
     *             bounding rectangle.
     * @param font - The display font of the <code>String</code>
     * @see java.awt.Graphics
     * @see java.awt.Rectangle
     * @see java.lang.String
     */
    public static void centerString(Graphics g, Rectangle r, String s, Font font) {
        FontRenderContext frc = new FontRenderContext(null, true, true);

        Rectangle2D r2D = font.getStringBounds(s, frc);
        int rWidth = (int) Math.round(r2D.getWidth());
        int rHeight = (int) Math.round(r2D.getHeight());
        int rX = (int) Math.round(r2D.getX());
        int rY = (int) Math.round(r2D.getY());

        int a = (r.width / 2) - (rWidth / 2) - rX;
        int b = (r.height / 2) - (rHeight / 2) - rY;

        g.setFont(font);
        g.drawString(s, r.x + a, r.y + b);
    }



    public static boolean isInsideCircle(double cX, double cY, double r, double pX, double pY) {
        double dist = Math.sqrt(Math.pow(cX - pX, 2) + Math.pow(cY - pY, 2));
        return dist <= r;
    }

    public static String timeConvert(long time) {
        return time / 60 / 60 / 24 + "d " + time / 60 / 60 % 24 + "h " + time / 60 % 60 + "m";
    }
}
