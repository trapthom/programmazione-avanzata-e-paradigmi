package pap.exam.thomas.common;

/**
 * Created by thomas on 24/04/17.
 */
public class ScaleFactor {
    private int factor = 0;
    private double baseScale = 1;

    public void scaleUp() {
        factor++;
    }

    public void scaleDown() {
        factor--;
    }

    public double getScale() {
        return baseScale * getScaleMultiplier();
    }

    public void updateBaseScale(double baseScale) {
        this.baseScale = baseScale;
    }

    public double getScaleMultiplier() {
        return Math.pow(1.1, factor);
    }
}
