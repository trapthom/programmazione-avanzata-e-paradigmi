package pap.exam.thomas.common;

import pap.exam.thomas.entitites.BodyPlane;
import pap.exam.thomas.entitites.BodyPlaneBuilder;

import java.util.Random;

/**
 * Created by thomas on 20/04/17.
 */
public class BodiesConstant {
    public static final double EARTH_MASS = 5.972D * Math.pow(10, 24);
    public static final double MOON_MASS = 7.34767309D * Math.pow(10, 22);
    public static final double MOON_EARTH_SPEED = 1023.056;
    public static final double MOON_EARTH_DISTANCE = 392.8 * Math.pow(10, 6);


    public static final BodyPlane BP_EARTH_MOON() {
        return new BodyPlaneBuilder()
                .width((int) (800 * Math.pow(10, 6)))
                .height((int) (800 * Math.pow(10, 6)))
                .addBody(EARTH_MASS, 0, 0, 0, 0)
                .addBody(MOON_MASS, 0, MOON_EARTH_DISTANCE, -MOON_EARTH_SPEED, 0)
                .build();
    }

    public static final BodyPlane BP_EARTH_2MOON() {
        return new BodyPlaneBuilder()
                .width((int) (800 * Math.pow(10, 6)))
                .height((int) (800 * Math.pow(10, 6)))
                .addBody(EARTH_MASS, 0, 0, 0, 0)
                .addBody(MOON_MASS, 0, MOON_EARTH_DISTANCE, -MOON_EARTH_SPEED, 0)
                .addBody(MOON_MASS, MOON_EARTH_DISTANCE, 0, 0, MOON_EARTH_SPEED)
                .build();
    }


    public static final BodyPlane BP_EARTH_3MOON() {
        return new BodyPlaneBuilder()
                .width((int) (800 * Math.pow(10, 6)))
                .height((int) (800 * Math.pow(10, 6)))
                .addBody(EARTH_MASS, 0, 0, 0, 0)
                .addBody(MOON_MASS, 0, MOON_EARTH_DISTANCE, -MOON_EARTH_SPEED, 0)
                .addBody(MOON_MASS, MOON_EARTH_DISTANCE, MOON_EARTH_DISTANCE, -MOON_EARTH_SPEED, 0)
                .addBody(MOON_MASS, MOON_EARTH_DISTANCE, 0, 0, MOON_EARTH_SPEED)
                .build();
    }


    public static final BodyPlane BP_RANDOM_BODIES(int count) {
        BodyPlaneBuilder builder = new BodyPlaneBuilder()
                .width((int) (800 * Math.pow(10, 6)))
                .height((int) (800 * Math.pow(10, 6)));

        Random rnd = new Random();

//        builder.addBody(5.972D * Math.pow(10, 24), 0, 0, 0, 0);
        for (int i = 0; i < count; i++) {
            double deltaMass = (rnd.nextDouble()) * 2; //max 25%
            double deltaPosX = (rnd.nextDouble() - 0.5) * 2; //max 50%
            double deltaPosY = (rnd.nextDouble() - 0.5) * 2; //max 50%
            double deltaSpeedX = (rnd.nextDouble() - 0.5) * 2; //max 50%
            double deltaSpeedY = (rnd.nextDouble() - 0.5) * 2; //max 50%
            builder.addBody(
                    BodiesConstant.MOON_MASS * deltaMass,
                    BodiesConstant.MOON_EARTH_DISTANCE * deltaPosX,
                    BodiesConstant.MOON_EARTH_DISTANCE * deltaPosY,
                    BodiesConstant.MOON_EARTH_SPEED * deltaSpeedX,
                    BodiesConstant.MOON_EARTH_SPEED * deltaSpeedY);
        }

        return builder.build();
    }
}
