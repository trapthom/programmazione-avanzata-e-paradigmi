package pap.exam.thomas;

import pap.exam.thomas.multithreading.BodyCalculationImpl;

public class Es5 {

    public static void main(String[] args) {
        new Simulator(new BodyCalculationImpl()).startGui();
    }

}
