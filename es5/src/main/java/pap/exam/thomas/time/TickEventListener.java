package pap.exam.thomas.time;

/**
 * Created by thomas on 17/04/17.
 */
public interface TickEventListener {
    void onTick(TickEvent e);
}
