package pap.exam.thomas.time;

/**
 * Created by thomas on 17/04/17.
 */
public class TickEvent {
    private long tick;

    public TickEvent(long tick) {
        this.tick = tick;
    }

    public long getTick() {
        return tick;
    }

    public void setTick(long tick) {
        this.tick = tick;
    }
}

