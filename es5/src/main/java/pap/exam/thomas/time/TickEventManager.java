package pap.exam.thomas.time;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by thomas on 17/04/17.
 */
public class TickEventManager {

    private static final TickEventManager INSTANCE = new TickEventManager();
    public static TickEventManager get() {
        return INSTANCE;
    }

    private int currentTick = 0;

    private List<TickEventListener> listeners = new ArrayList<>();

    private TickEventManager() {
    }

    public void addListener(TickEventListener listener) {
        listeners.add(listener);
    }

    public void tick() {
        TickEvent e = new TickEvent(currentTick);
        listeners.parallelStream().forEach(te -> te.onTick(e));
        currentTick++;
    }

    public void reset() {
        currentTick = 0;
    }

    public int getCurrentTick() {
        return currentTick;
    }
}
