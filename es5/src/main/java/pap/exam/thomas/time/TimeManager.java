package pap.exam.thomas.time;

/**
 * Created by thomas on 17/04/17.
 */
public class TimeManager {
    private static final TimeManager INSTANCE = new TimeManager();

    public static TimeManager get() {
        return INSTANCE;
    }

    private TimeManager() {
    }

    private boolean run = false;

    private void loop() {
        Runnable runnable = () -> {
            while (run) {
                TickEventManager.get().tick();
            }
        };
        new Thread(runnable).start();
    }

    public void start() {
        if (run) return;
        run = true;
        loop();
    }

    public void pause() {
        run = false;
    }

    public void stop() {
        TickEventManager.get().reset();
        run = false;
    }


}
