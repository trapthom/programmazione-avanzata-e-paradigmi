package pap.exam.thomas;

import pap.exam.thomas.common.BodiesConstant;
import pap.exam.thomas.common.ScaleFactor;
import pap.exam.thomas.common.Utils;
import pap.exam.thomas.entitites.Body;
import pap.exam.thomas.entitites.BodyPlane;
import pap.exam.thomas.gui.GuiHandler;
import pap.exam.thomas.gui.GuiItems;
import pap.exam.thomas.gui.GuiManager;
import pap.exam.thomas.multithreading.BodiesForceCalculationException;
import pap.exam.thomas.multithreading.BodyCalculation;
import pap.exam.thomas.multithreading.BodyCalculationException;
import pap.exam.thomas.time.TickEventManager;
import pap.exam.thomas.time.TimeManager;

import javax.swing.table.DefaultTableModel;
import java.awt.*;
import java.awt.event.ItemEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.text.DecimalFormat;
import java.util.Optional;

/**
 * Created by thomas on 22/04/17.
 */
public class Simulator implements GuiHandler {
    public static final int TICK_SIZE = 10;

    private static BodyPlane bp;

    private ScaleFactor zoomScale = new ScaleFactor();
    private ScaleFactor speedScale = new ScaleFactor();

    private BodyCalculation bodyCalculation;

    private long elapsedTime = 0;

    private Body selectedBody = null;

    private DecimalFormat formatter = new DecimalFormat("0.###E0");

    private GuiItems items;

    public Simulator(BodyCalculation bodyCalculation) {
        this.bodyCalculation = bodyCalculation;
    }


    public void startGui() {
        TickEventManager.get().addListener(e -> System.out.println("A: TICK: " + e.getTick()));
        GuiManager.get().renderGui(this);
    }

    protected void onStart() {
        TimeManager.get().start();
        updateUI(items);
        items.getExampleCombobox().setEnabled(false);
    }

    protected void onStop() {
        bp = buildBodyPlane((String) items.getExampleCombobox().getSelectedItem());
        TimeManager.get().stop();
        updateUI(items);
        items.getExampleCombobox().setEnabled(true);
    }

    protected void onPause() {
        TimeManager.get().pause();
        updateUI(items);
    }

    public void applyHandler(GuiItems items) {
        this.items = items;

        //handle startGui/stop/pause button
        items.getBtnStart().addActionListener(ae -> onStart());
        items.getBtnStop().addActionListener(ae -> onStop());
        items.getBtnPause().addActionListener(ae -> onPause());

        //handle zoom button
        items.getBtnZoomIn().addActionListener(ae -> {
            zoomScale.scaleUp();
            updateUI(items);
        });
        items.getBtnZoomOut().addActionListener(ae -> {
            zoomScale.scaleDown();
            updateUI(items);
        });

        //handle speed button
        items.getBtnSpeedUp().addActionListener(ae -> {
            speedScale.scaleUp();
            updateUI(items);
        });
        items.getBtnSpeedDown().addActionListener(ae -> {
            speedScale.scaleDown();
            updateUI(items);
        });

        items.getExampleCombobox().addItem("Earth-Moon");
        items.getExampleCombobox().addItem("Earth-2Moon");
        items.getExampleCombobox().addItem("Earth-3Moon");
        items.getExampleCombobox().addItem("10 Random bodies");
        items.getExampleCombobox().addItem("20 Random bodies");
        items.getExampleCombobox().addItem("100 Random bodies");
        items.getExampleCombobox().addItem("1000 Random bodies");

        items.getExampleCombobox().addItemListener(e -> {
            if (e.getStateChange() == ItemEvent.SELECTED) {
                bp = buildBodyPlane((String) e.getItem());
                updateUI(items);
            }
        });

        //handle drawing
        items.getDrawingArea().setGraphicsDraw((g, w, h) -> {
//            g.clearRect(0, 0, Integer.MAX_VALUE, Integer.MAX_VALUE);
            drawBodyPlane(bp, g, h, w);

            g.setColor(Color.BLUE);
            g.drawLine(10, 10, 10, h - 20);
            g.drawString("Y", 30, h - 20);

            g.setColor(Color.RED);
            g.drawLine(10, 10, w - 20, 10);
            g.drawString("X", w - 20, 30);
        });

        items.getDrawingArea().addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                Optional<Body> sel = findBodySelected(bp, e.getPoint(), items.getDrawingArea().getWidth(), items.getDrawingArea().getHeight());
                if (sel.isPresent()) {
                    selectedBody = sel.get();
                } else {
                    selectedBody = null;
                }

                updateUI(items);
            }
        });

        //Bind computation to time
        TickEventManager.get().addListener(e -> {
            try {
                elapsedTime += getTick();
                bodyCalculation.calc(bp.getBodyPositions(), getTick());
            } catch (BodyCalculationException e1) {
                e1.printStackTrace();
            } catch (BodiesForceCalculationException e1) {
                e1.printStackTrace();
            }

            updateUI(items, e.getTick() % 100 == 0);
        });

        bp = buildBodyPlane((String) items.getExampleCombobox().getSelectedItem());
        updateUI(items);

    }


    private void updateUI(GuiItems items) {
        updateUI(items, true);
    }

    private void updateUI(GuiItems items, boolean updateInfo) {
        {
            if (updateInfo) {
                DefaultTableModel model = (DefaultTableModel) items.getTable().getModel();
                model.setNumRows(0);
                model.addRow(new Object[]{"ThreadCount", Thread.activeCount()});
                model.addRow(new Object[]{"Tick", TickEventManager.get().getCurrentTick()});
                model.addRow(new Object[]{"Time", Utils.timeConvert(elapsedTime)});
                model.addRow(new Object[]{"Zoom Multiplier", String.format("%.2f X", zoomScale.getScaleMultiplier())});
                model.addRow(new Object[]{"Speed Multiplier", String.format("%.2f X", speedScale.getScaleMultiplier())});

                if (selectedBody != null) {
                    model.addRow(new Object[]{"", ""});
                    model.addRow(new Object[]{"Body Id", selectedBody.getId()});
                    model.addRow(new Object[]{"Body Mass", formatter.format(selectedBody.getMass())});
                    model.addRow(new Object[]{"Body X", formatter.format(selectedBody.getX())});
                    model.addRow(new Object[]{"Body Y", formatter.format(selectedBody.getY())});
                    model.addRow(new Object[]{"Body Vx", formatter.format(selectedBody.getVx())});
                    model.addRow(new Object[]{"Body Vy", formatter.format(selectedBody.getVy())});
                }
            }
            items.getDrawingArea().updateUI();
        }
    }

    private Optional<Body> findBodySelected(BodyPlane bp, Point point, double w, double h) {

        return bp.getBodyPositions().stream()
                .filter(b -> Utils.isInsideCircle(
                        b.getX() * zoomScale.getScale() + w / 2,
                        b.getY() * zoomScale.getScale() + h / 2,
                        10,
                        point.getX(),
                        point.getY()))
                .findFirst();
    }


    private int getTick() {
        return (int) (TICK_SIZE * speedScale.getScale());
    }

    private static BodyPlane buildBodyPlane(String request) {
        switch (request) {
            case "Earth-Moon":
                return BodiesConstant.BP_EARTH_MOON();
            case "Earth-2Moon":
                return BodiesConstant.BP_EARTH_2MOON();
            case "Earth-3Moon":
                return BodiesConstant.BP_EARTH_3MOON();
            case "10 Random bodies":
                return BodiesConstant.BP_RANDOM_BODIES(10);
            case "20 Random bodies":
                return BodiesConstant.BP_RANDOM_BODIES(20);
            case "100 Random bodies":
                return BodiesConstant.BP_RANDOM_BODIES(100);
            case "1000 Random bodies":
                return BodiesConstant.BP_RANDOM_BODIES(1000);
            default:
                return null;
        }

    }


    private void drawBodyPlane(BodyPlane bodyPlane, Graphics g, int h, int w) {
        if (bodyPlane == null) return;
        float xScale = (float) w / (float) (bodyPlane.getWidth());
        float yScale = (float) h / (float) (bodyPlane.getHeight());

        zoomScale.updateBaseScale(Math.min(xScale, yScale));

        for (Body b : bodyPlane.getBodyPositions()) {
            int x = (int) (b.getX() * zoomScale.getScale() + w / 2);
            int y = (int) (b.getY() * zoomScale.getScale() + h / 2);
            int size = 20;//estimateSize(b);
            if (b == selectedBody) {
                g.setColor(Color.BLUE);
                g.fillOval(x - size / 2, y - size / 2, size, size);
                g.setColor(Color.BLACK);
            }
            g.drawOval(x - size / 2, y - size / 2, size, size);
            Utils.centerString(g, new Rectangle(x - size / 2, y - size / 2, size, size), b.getId() + "", g.getFont());

        }


    }

}
