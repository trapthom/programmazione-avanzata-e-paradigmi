package pap.exam.thomas.common;

import org.junit.Assert;
import org.junit.Test;
import pap.exam.thomas.entitites.Body;
import pap.exam.thomas.entitites.DoubleBidimentionalVector;

import java.util.List;

/**
 * Created by thomas on 22/04/17.
 */
public class PhysicsUtilsTest {
    @Test
    public void computeDist() throws Exception {
        double res = PhysicsUtils.computeDist(0, 0, 0, 0);
        Assert.assertEquals(res, PhysicsUtils.MIN_DIST, 0.1);

        res = PhysicsUtils.computeDist(0, 0, 8 * Math.pow(10, 10), 0);
        Assert.assertEquals(res, 8 * Math.pow(10, 10), 0.1);

        res = PhysicsUtils.computeDist(0, 0, 0, 8 * Math.pow(10, 10));
        Assert.assertEquals(res, 8 * Math.pow(10, 10), 0.1);

        res = PhysicsUtils.computeDist(0, 3 * Math.pow(10, 10), 0, 4 * Math.pow(10, 10));
        Assert.assertEquals(res, 5 * Math.pow(10, 10), 0.1);

    }

    @Test
    public void computeForce() throws Exception {
        List<Body> bodies = BodiesConstant.BP_EARTH_MOON().getBodyPositions();
        DoubleBidimentionalVector v = PhysicsUtils.computeForce(bodies.get(0), bodies.get(1));
        Assert.assertEquals(v.getModule(), 1.89 * Math.pow(10, 20), Math.pow(10, 20));
        Assert.assertTrue(v.getY() > 0);
        v = PhysicsUtils.computeForce(bodies.get(1), bodies.get(0));
        Assert.assertTrue(v.getY() < 0);

        bodies = BodiesConstant.BP_EARTH_2MOON().getBodyPositions();
        v = PhysicsUtils.computeForce(bodies.get(0), bodies.get(2));
        Assert.assertEquals(v.getModule(), 1.89 * Math.pow(10, 20), Math.pow(10, 20));
        Assert.assertTrue(v.getX() > 0);
        v = PhysicsUtils.computeForce(bodies.get(2), bodies.get(0));
        Assert.assertTrue(v.getX() < 0);
    }

}
