package pap.exam.thomas.common;

import org.junit.Assert;
import org.junit.Test;

/**
 * Created by thomas on 26/04/17.
 */
public class UtilsTest {
    @Test
    public void isInsideCircle() throws Exception {
        boolean res = Utils.isInsideCircle(0, 0, 100, 100, 0);
        Assert.assertTrue(res);

        res = Utils.isInsideCircle(0, 0, 100, 110, 0);
        Assert.assertFalse(res);

        res = Utils.isInsideCircle(0, 0, 100, 0, 100);
        Assert.assertTrue(res);

        res = Utils.isInsideCircle(0, 0, 100, 0, 110);
        Assert.assertFalse(res);

        res = Utils.isInsideCircle(100, 100, 10, 105, 105);
        Assert.assertTrue(res);

        res = Utils.isInsideCircle(100, 100, 10, 105, 105);
        Assert.assertTrue(res);
    }

}
