import Test.Hspec
import Test.QuickCheck
import Control.Exception (evaluate)
import Es1

main :: IO ()
main = hspec $ do
  describe "Array of " $ do

    it "[Dot,Dot,Dot] should return 0" $ do
       maxSubSeq [Dot,Dot,Dot] `shouldBe` Just (0 :: Int)

    it "[Square, Dot,Dot,Dot] should return 1" $ do
       maxSubSeq [Square, Dot,Dot,Dot] `shouldBe` Just (1 :: Int)

    it "[Square, Square, Dot,Dot,Dot] should return 2" $ do
       maxSubSeq [Square, Square, Dot,Dot,Dot] `shouldBe` Just (2 :: Int)

    it "[Square, Dot, Square, Square, Dot,Dot,Dot] should return 4" $ do
       maxSubSeq [Square, Dot, Square, Square, Dot,Dot,Dot] `shouldBe` Just (4 :: Int)

    it "[Square, Dot, Square, Square, Square, Square, Dot,Dot,Dot,Dot,Dot] should return 6" $ do
       maxSubSeq [Square, Dot, Square, Square, Square, Square, Dot,Dot,Dot,Dot,Dot] `shouldBe` Just (6 :: Int)

    it "[Square, Dot, Square, Square, Square, Square, Dot,Dot] should return 6" $ do
       maxSubSeq [Square, Dot, Square, Square, Square, Square, Dot,Dot] `shouldBe` Just (6 :: Int)

    it "[Square] should return Nothing" $ do
       maxSubSeq [Square] `shouldBe` (Nothing :: Maybe Int)

    it "[Dot] should return 0" $ do
       maxSubSeq [Dot] `shouldBe` Just (0 :: Int)

    it "Empty should return Nothing" $ do
       maxSubSeq [] `shouldBe` (Nothing :: Maybe Int)

