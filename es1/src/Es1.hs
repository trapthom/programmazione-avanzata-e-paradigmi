module Es1 where
import System.IO
import Data.Char

--Definisco il tipo  Elem
data Elem = Dot | Square

--Definisco la funzione di ugualianza per i tipi Elem
elemEq :: Elem -> Elem -> Bool
elemEq Dot      Dot     = True
elemEq Square   Square  = True
elemEq _        _       = False

--Definisco la funzione maxSeq che accetta in input:
-- * un array di Elem
-- * valore della sequenza corrente
-- * valore della sequenza migliore trovata
-- * indice della sequenza corrente
-- * indice della sequenza migliore trovata
-- * indice dell'elemento corrente dell'array

maxSeq :: [Elem] -> Int -> Int -> Int -> Int -> Int -> Maybe Int
--Caso banale di array vuoto:
maxSeq [] curi maxi curIndex maxIndex index = Nothing
--Caso banale di array con un solo elemento:
--In questo caso se le sequenze sono vuote (ovvero nn ho trovato nessun elemento nell'array)
--e se l'elemento corrente non è un Dot allora ritorno -1
--In caso contrario ritorno l'indice con il miglior valore di sequenza
maxSeq [a] curi maxi curIndex maxIndex index | maxi == 0 && curi == 0 &&  not(elemEq a Dot) = Nothing
                                             | curi > maxi = Just curIndex
                                             | otherwise = Just maxIndex
--Caso ricorsivo:
--In questo caso chiamo a0 e a1 i primi due elementi della sequenza
--Se entrambi sono Dot allora proseguo la ricorsione eliminando il primo elemento dell'array (a0) e incrementando il valore della sequenza corrente
--In caso contrario verifico se il valore corrente rappresenta un miglioramento, in tal caso aggiorno maxi e maxIndex in ogni caso azzero la sequenza corrente
--Ovviamente aumento sempre l'indice corrente di 1
maxSeq (a0:a1:as) curi maxi curIndex maxIndex index
            | elemEq a0 Dot && elemEq a1 Dot = maxSeq (a1:as) (curi+1) maxi curIndex maxIndex (index+1)
            | curi > maxi = maxSeq (a1:as) 0 curi (index+1) curIndex  (index+1)
            | otherwise =  maxSeq (a1:as) 0 maxi (index+1) maxIndex (index+1)

--Definisco la funzion maxSubSeq che funge da punto di ingresso e avvia la ricorsione con i valori di default
maxSubSeq :: [Elem] -> Maybe Int
maxSubSeq xs = maxSeq xs 0 0 0 0 0