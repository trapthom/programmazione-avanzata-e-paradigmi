module Es2 where
import System.IO
import Data.Char

--Definisco il tipo  Elem
data Elem = Dot | Square

--Definisco la funzione di ugualianza per i tipi Elem
elemEq :: Elem -> Elem -> Bool
elemEq Dot      Dot     = True
elemEq Square   Square  = True
elemEq _        _       = False

--Definisco il tipo  BTree
data BTree a = Empty | Node a (BTree a) (BTree a)


maybeSum :: Maybe Int -> Maybe Int -> Maybe Int
maybeSum Nothing b = Nothing
maybeSum a Nothing = Nothing
maybeSum (Just a) (Just b) = Just (a + b)

--Definisco la funzione countSquare che accetta in input:
-- * un BTree di Elem
-- * livello corrente
-- * livello a cui contare il numero di square
--E ritorna:
-- * Nothing nel caso il livello richiesto non sia presente nell'albero
-- * il numero di square del livello in caso contrario

countSquare :: BTree Elem -> Int -> Int -> Maybe Int
--Caso base, se il livello è Empty torno -1:
countSquare Empty curLevel reqLevel = Nothing
--Se il livello corrente è uguale al livello richiesto e il nodo corrente è Square torna 1
countSquare (Node a l r) curLevel reqLevel | curLevel == reqLevel && elemEq a Square = Just 1
--Se il livello corrente è uguale al livello richiesto ma non ci sono Square torna 0
                                         | curLevel == reqLevel = Just 0
--Se non sono ancora al livello richiesto analizzo i due sottorami, se uno torna -1 lo ignoro (finito l'albero),
--se entrambi sono -1 allora il livello richiesto non è presente in questo sottoramo e torno -1
                                         | r1 == Nothing && r2 == Nothing = Nothing
                                         | r1 == Nothing = r2
                                         | r2 == Nothing = r1
                                         | otherwise = maybeSum r1 r2
                                   where
                                      r1 = (countSquare l (curLevel+1) reqLevel)
                                      r2 = (countSquare r (curLevel+1) reqLevel)


--Definisco la funzione countMax che accetta in input:
-- * un BTree di Elem
-- * il livello con il numero massimo di Square trovato fin'ora
-- * il numero massimo di Square trovato fin'ora su un solo livello
-- * livello corrente da analizzare
--E ritorna:
-- * -1 se non sono presenti Square nell'albero
-- * il livello con il numero massimo di square

countMax :: BTree Elem -> Maybe Int -> Maybe Int -> Int -> Maybe Int
countMax tree maxLevel maxCount curLevel |  r == Nothing && maxCount == Just 0 = Nothing
                                         |  r == Nothing = maxLevel
                                         |  r <= maxCount =  countMax tree maxLevel maxCount (curLevel+1)
                                         |  r > maxCount =  countMax tree (Just curLevel) r (curLevel+1)
                                    where
                                        r = countSquare tree 0 curLevel

--Definisco la funzion levelWithMaxDots che funge da punto di ingresso e avvia la ricorsione con i valori di default
levelWithMaxDots :: BTree Elem -> Maybe Int
levelWithMaxDots t = countMax t Nothing Nothing 0
