import Test.Hspec
import Test.QuickCheck
import Control.Exception (evaluate)
import Es2

main :: IO ()
main = hspec $ do
  describe "Tree of " $ do

    it "(Node Dot (Node Dot (Node Square Empty Empty) (Node Dot (Node Dot Empty Empty) (Node Dot Empty Empty))) (Node Square (Node Square Empty (Node Dot Empty Empty))Empty)) should return 2" $ do
       levelWithMaxDots (Node Dot (Node Dot (Node Square Empty Empty) (Node Dot (Node Dot Empty Empty) (Node Dot Empty Empty))) (Node Square (Node Square Empty (Node Dot Empty Empty))Empty)) `shouldBe` Just (2 :: Int)

    it "(Node Dot (Node Dot (Node Square Empty Empty) (Node Dot (Node Dot Empty (Node Square Empty Empty)) (Node Dot Empty (Node Square Empty Empty)) ) ) (Node Square (Node Square Empty (Node Dot Empty (Node Square Empty Empty)) ) Empty ) ) should return 4" $ do
       levelWithMaxDots (Node Dot (Node Dot (Node Square Empty Empty) (Node Dot (Node Dot Empty (Node Square Empty Empty)) (Node Dot Empty (Node Square Empty Empty)) ) ) (Node Square (Node Square Empty (Node Dot Empty (Node Square Empty Empty)) ) Empty ) ) `shouldBe` Just (4 :: Int)

    it "(Node Dot Empty Empty) should return Nothing" $ do
       levelWithMaxDots (Node Dot Empty Empty) `shouldBe` (Nothing :: Maybe Int)

    it "(Node Dot (Node Dot Empty Empty) (Node Dot Empty Empty)) should return Nothing" $ do
       levelWithMaxDots (Node Dot (Node Dot Empty Empty) (Node Dot Empty Empty)) `shouldBe` (Nothing :: Maybe Int)

    it "(Node Square Empty Empty) should return 0" $ do
       levelWithMaxDots (Node Square Empty Empty) `shouldBe` Just (0 :: Int)

    it "countSquare (Node Dot Empty Empty) 0 0 should return 0" $ do
       countSquare (Node Dot Empty Empty) 0 0 `shouldBe` Just (0 :: Int)
