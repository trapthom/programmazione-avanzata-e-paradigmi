module Es3 where
import System.IO
import Data.Char

average arr = a / fromIntegral b
                where
                a=sum arr
                b=length arr


--Definisco la funzion levelWithMaxDots che funge da punto di ingresso e avvia la ricorsione con i valori di default
--printVariance :: [Float] -> Float
printVariance t = mapM_ (print) (map (-avg+) (filter (>avg) [5.0, 7.0, 6.0, 9.0, 1.0, 2.0]))
                where
                avg = average t