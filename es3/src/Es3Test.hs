import Test.Hspec
import Test.QuickCheck
import Control.Exception (evaluate)
import Es3

main :: IO ()
main = hspec $ do
  describe "Tree of " $ do

    it "printVariance ([5.0, 7.0, 6.0, 9.0, 1.0, 2.0]) should print 2 1 4" $ do
       printVariance ([5.0, 7.0, 6.0, 9.0, 1.0, 2.0])